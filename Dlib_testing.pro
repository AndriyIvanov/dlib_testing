QT += core
QT -= gui

TARGET = Dlib_testing
CONFIG += console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++14
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH +='/usr/include/aarch64-linux-gnu/qt5/'

CONFIG += link_pkgconfig
#opencv
#INCLUDEPATH += `pkg-config --cflags opencv`
#LIBS += `pkg-config --libs opencv`
PKGCONFIG += opencv

#Dlib
PKGCONFIG += dlib-1

TEMPLATE = app

SOURCES += main.cpp

