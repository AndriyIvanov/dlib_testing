
#include <iostream>
#include <string>

#include <chrono>
#include <QElapsedTimer>
#include <QByteArray>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/tracking.hpp>

#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/dir_nav.h>


using namespace std;
using namespace cv;
using namespace dlib;

#define SSTR(x) static_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()


int main()
{

    const string pathToVideo ("/home/nvidia/Projects/TrackingTest/trackingtest/Data/Run 100 m.mp4");

    VideoCapture video(pathToVideo);

    // Exit if video is not opened
    if(!video.isOpened())
    {
       cout << "Could not read video file" << endl;
       return EXIT_FAILURE;
    }

    // Read first frame
    Mat frame;
    if (!video.read(frame))
    {
       cout << "Cannot read video file" << endl;
       return EXIT_FAILURE;
    }

    Ptr<Tracker> tracker = TrackerKCF::create();
    Rect2d in_box = selectROI(frame);
    destroyWindow("ROI selector");
    tracker->init(frame, in_box);
    Rect2d out_box;

    while(video.read(frame))
    {
       double timer = (double)getTickCount();
       bool ok = tracker->update(frame, out_box);
       if (ok)
       {
           cv::rectangle(frame, out_box, cv::Scalar(0,0,255), 3, 1);
       }
       else
       {
           putText(frame, "Tracking failed", Point(10,70), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,0), 2);
       }


       float fps = getTickFrequency() / ((double)getTickCount() - timer);

       // Display FPS on frame
       putText(frame, "FPS : " + SSTR(int(fps)), Point(10,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,0), 2);

       // Display frame.
       imshow("Tracker", frame);

       // Exit if ESC pressed.
       if(waitKey(1) == 27) break;
    }
}


